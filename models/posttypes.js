'use strict';
module.exports = function(sequelize, DataTypes) {
  var PostTypes = sequelize.define('PostTypes', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return PostTypes;
};