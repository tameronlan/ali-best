'use strict';
module.exports = function(sequelize, DataTypes) {
  var Selection = sequelize.define('Selection', {
    title: DataTypes.STRING,
    infoBefore: DataTypes.TEXT,
    infoAfter: DataTypes.TEXT,
    typePostId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Selection;
};