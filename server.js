/* external code require */
let Sequelize           = require("sequelize");
let gzipStatic          = require('connect-gzip-static');
let express             = require("express");
let cookieParser        = require("cookie-parser");
let bodyParser          = require("body-parser");
let app = express();

/* my code require */
let Router              = require(__dirname + '/core/Router');
let appInfo             = require(__dirname + '/core/config');

/* Addition for global object */
Object.assign(global, {
    appInfo: appInfo,
    app: app,
    appPath: __dirname,
    sequelize: new Sequelize(appInfo.getSequelizeUrl()),
    models: {},
    helpers: {}
});

/* engine for render 'ejs'. More info: http://www.embeddedjs.com/ */
app.set('view engine', "ejs");

/* delete x-powered header*/
app.disable('x-powered-by');

/* supporting cookie */
app.use(cookieParser());

//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* static url like /static/base/img will be translate to __dirname + '/public/static/base/img' */
app.use('/static', express.static(__dirname + '/public/static'));

/* custom router for searching Controller, action for request */
Router.do();

/* listen port */
app.listen(appInfo.port);

sequelize.sync().then(() =>{
    console.log("sequelize connected to db: " + appInfo.dbConfig.database);


    console.log("Listening on port " + appInfo.port);
});