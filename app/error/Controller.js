let BaseController = require(`${appPath}/core/Controller`);

class ErrorController extends BaseController{
    constructor(props){
        super(props);
    }

    error404(){
        this.render("404");
    }

    error500(){
        this.render("500");
    }
}

module.exports = ErrorController;