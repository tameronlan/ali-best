let BaseController = require(`${appPath}/core/Controller`);

class AboutController extends BaseController{
    constructor(props){
        super(props);
    }

    index(){
        this.data.title = 'Главная страница';

        this.render("index");
    }
}

module.exports = AboutController;