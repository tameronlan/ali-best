let BaseController = require(`${appPath}/core/Controller`);

class ContactControllerIndex extends BaseController{
    constructor(props){
        super(props);
    }

    index(){
        this.render("index");
    }
}

module.exports = ContactControllerIndex;