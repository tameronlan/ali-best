let BaseController = require(`${appPath}/core/Controller`);

class UserControllerIndex extends BaseController{
    constructor(props){
        super(props);
    }

    index(){
        this.render("index");
    }

    profile(){
        this.render("profile");
    }

    login(){
        this.render("login");
    }

    doLogin(){
        if ( this.isAjax ){
            this.result.json({
                success: 1
            });
        } else {
            this.result.redirect('user/profile');
        }
    }

    signup(){
        this.appendScript('http://malsup.github.com/jquery.form.js');
        this.data.title = 'Регистрация';

        this.render("signup");
    }

    doSignup(){
        this.result.send(this.request.body);

        // if ( this.isAjax ){
        //     this.result.json({
        //         success: 1
        //     });
        // } else {
        //     this.result.redirect('user/profile');
        // }
    }
}

module.exports = UserControllerIndex;