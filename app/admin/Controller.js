let BaseController = require(`${appPath}/core/Controller`);

class AdminController extends BaseController{
    constructor(props){
        super(props);
    }

    index(){
        let shell = require("shelljs");

        let brunchesCommand = shell.exec('git branch', {silent:true});

        let branchesText = brunchesCommand.stdout;
        let branchesSplited = branchesText.split('\n');
        let currentBranch = 'master';
        let branches = [];

        if (brunchesCommand.code !== 0) {
            shell.echo('bad news');
        } else {
            delete branchesSplited[branchesSplited.length - 1];

            branchesSplited.forEach(text => {
                let branch = text.replace(/\s/gi, '');

                if ( branch.length &&  branch[0] === '*' ){
                    branch = branch.slice(1);

                    currentBranch = branch;
                }

                branches.push(branch);
            });
        }

        this.data.title = 'Панель администратора';
        this.data.branches = branches;
        this.data.currentBranch = currentBranch;

        this.render("index");
    }

    icons(){
        this.render("icons");
    }
}

module.exports = AdminController;