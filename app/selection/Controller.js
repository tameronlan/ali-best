let BaseController = require(`${appPath}/core/Controller`);

class SelectionsController extends BaseController{
    constructor(props){
        super(props);
    }

    index(){
        this.data.title = 'Подборки товаров';

        this.render("index");
    }
}

module.exports = SelectionsController;