let fs = require('fs');
let env = {};

if ( fs.existsSync('core/env/base.json') ) {
    Object.assign(env, require('./env/base.json'));
}

if ( fs.existsSync('core/env/database.json') ) {
    Object.assign(env, {
        db: require('./env/database.json')
    });
}

const STATE_APP_PRODUCTION = 'prod';
const STATE_APP_STAGE = 'stage';
const STATE_APP_LOCAL = 'local';

module.exports = {
    dbConfig: env.db || {},
    state: STATE_APP_LOCAL,
    port: env.port ? env.port : 83,
    isProduction(){
        return this.state === STATE_APP_PRODUCTION;
    },
    isLocal(){
        return this.state === STATE_APP_LOCAL;
    },
    isStage(){
        return this.state === STATE_APP_STAGE;
    },
    getStates(){
        return [STATE_APP_PRODUCTION, STATE_APP_LOCAL, STATE_APP_STAGE];
    },
    getSequelizeUrl(){
        let dbConfig = this.dbConfig;

        return `${dbConfig.dialect}://${dbConfig.user}:${dbConfig.password}@${dbConfig.host}:${dbConfig.port}/${dbConfig.database}`;
    },
    'STATE_APP_PRODUCTION': STATE_APP_PRODUCTION,
    'STATE_APP_STAGE': STATE_APP_STAGE,
    'STATE_APP_LOCAL': STATE_APP_LOCAL,
};