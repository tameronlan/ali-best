class Controller {
    constructor(props = {}){
        this.dispatch(props);
    }

    dispatch(props = {}){
        this.request = props.request;
        this.result = props.result;
        this.params = props.request.params;
        this.next = props.next;
        this.path = props.path;
        this.data = this.result.locals;

        this.data.title = '';
        this.data.description = '';
        this.data.keywords = '';
        this.data.controller = '';
        this.data.action = '';
        this.data.scripts = [];
        this.data.css = [];
        this.data.path = this.path;

        app.set('views',  `${appPath}/app/layout`);
    }

    render(action = false, props = {}){
        this.data.pagePath = false;

        if ( props.disableLayout ){
            this.data.pagePath = [appPath, 'app', this.path, 'tpl', action].join('/');

            this.result.render(this.data.pagePath);
        } else {
            props.layout = props.layout || 'web';

            if ( action ){
                this.data.pagePath = [appPath, 'app', this.path, 'tpl', action].join('/');
            }

            this.result.render(props.layout);
        }
    }

    appendScript(scriptUrl){
        this.data.scripts.push(scriptUrl)
    }

    appendStyle(scriptUrl){
        this.data.scripts.push(scriptUrl)
    }
}

module.exports = Controller;