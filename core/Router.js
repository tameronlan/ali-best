let fs = require('fs');

class Router{
    static do(){
        // set root catalog
        app.all("/", (request, result, next) => {
            Router.setRoute('main', request, result, next);
        });

        app.all("/signup", (request, result, next) => {
            Router.setRoute('user.signup', request, result, next);
        });

        app.all("/login", (request, result, next) => {
            Router.setRoute('user.login', request, result, next);
        });

        // set route for controller/action urls
        app.all("/:controllerPath/:controllerName/:action", Router.getRoute);

        // set route for controller/action urls
        app.all("/:controllerPath/:action", Router.getRoute);

        // set route for only controller
        app.all("/:controllerPath/", Router.getRoute);

        app.all("*", Router.notFound);
    }

    static getRoute(request, result, next){
        let controllerPath = request.params.controllerPath;
        let controllerName = request.params.controllerName || 'Index';
        let actionName = request.params.action || 'index';
        let pathSingle = `${appPath}/app/${controllerPath}/Controller.js`;
        let pathFolder = `${appPath}/app/${controllerPath}/controller/${controllerName}.js`;
        let paths = [pathSingle, pathFolder];
        let controller = false;

        paths.some(path => {
            if ( fs.existsSync(path) ) {
                controller =  new (require(path))({
                    request,
                    result,
                    path: controllerPath
                });

                return true;
            }
        });

        if ( controller ){
            if ( controller[actionName] ){
                controller[actionName]();
            } else {
                Router.notFound(request, result, next);
            }
        } else {
            next();
        }
    }

    static setRoute(url = '', request, result, next){
        if ( url.length ){
            let partUrl = url.split('.');
            let controller = partUrl[0];
            let action = partUrl.length === 1 ? 'index' : partUrl[1];

            request.params.controllerPath = controller;
            request.params.action = action;

            Router.getRoute(request, result, next);
        } else {
            next();
        }
    }

    static notFound(request, result, next){
    }
}

module.exports = Router;